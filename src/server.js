const config = require("./config");
const express = require("express");
const mysql = require('mysql2');

let server = express();

db = mysql.createConnection({
    host: config.DB_HOST,
    user: config.DB_USER_NAME,
    password: config.DB_PASSWORD,
    database: config.DB_NAME
})
const loadMiddlewares = () => {
    server.use(express.json());
    server.use(express.static("public"));
}

const listen = () => {
    server.listen(config.PORT, () => {
        console.info(`Server is running on port ${config.PORT}.`);
    });
}

server.get("/api", async (req, res) => {
    // ... Fetch data from database and send in response
    const dataFromDatabase = {
        breed: "pug",
        cute: true,
        age: 2,
    };

    res.send(dataFromDatabase);
});


server.get("/products", async (req,res) => {
    let sql = `SELECT * FROM product`;
    db.query(sql,(err, data, fields) => {
        if (err) throw err;
        res.json({
            status: 200,
            data,
            message: `Retrieved list of products`
        })
    })
})
function start() {
    loadMiddlewares();
    listen();
}

module.exports = {
    start
};