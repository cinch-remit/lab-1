# Lab 1 Solution

## Database
My solution uses a `mysql` database. The name of the database is `lab1` and it has only one
table called `product`.

### Database Driver For Node JS
The database driver installed is called `mysql2`. Initially I installed `mysql` but
ran into problems with authentication. `mysql2` is optimized for the latest version 
of `mysql.`

### Bootstrap Data
In the folder dev-db-k8s, there is an sql file called init which can be used to bootstrap the database with information.
There exists a `kubernetes` manifest file which can be used to start a local mysql server with all the necessary data

## Views
This solution only has 1 view that loads all the products in a database and displays on the homepage. The button
`Load Products` uses a fetch call to retrieve the data.

## Environment Variables
There is a sample environment file which shows the kind of data which needs to be passed to a .env file.