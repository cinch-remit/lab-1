const button = document.getElementById("products");
const productContainer = document.getElementById("product-container");
let existingData = false;

button.addEventListener("click", () => {
    if (existingData) {
        return;
    }
    fetch("http://localhost:3000/products")
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            throw res
        })
        .then(data => {
            data.data.forEach(product => {
                const { id, description, image_url, name, price } = product;
                const productDiv = document.createElement('article');
                productDiv.setAttribute("id", id);
                productDiv.style.cssText = "mw5 center bg-white br3 pa3 pa4-ns mv3 ba b--black-10";

                const idParagraph = document.createElement('p');
                idParagraph.innerText = `id: ${id}`;

                const nameParagraph = document.createElement('p');
                nameParagraph.innerText = `name: ${name}`;

                const descriptionParagraph = document.createElement('p');
                descriptionParagraph.innerText = `description: ${description}`;

                const imageUrl = document.createElement('a');
                imageUrl.innerText = `see image`
                imageUrl.setAttribute("href", image_url);

                const priceParagraph = document.createElement('p');
                priceParagraph.innerText = `price: ${price}`;

                productDiv.append(idParagraph);
                productDiv.append(nameParagraph);
                productDiv.append(descriptionParagraph);
                productDiv.append(imageUrl);
                productDiv.append(priceParagraph);

                productContainer.append(productDiv);

                existingData = true;
            })
        })
        .catch(err => console.log(err));
})